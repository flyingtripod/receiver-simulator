#include <SPI.h>

//#define LOG_ERR
//#define DEBUG_ERR

#define DATA_LEN 16

#define UART_SET_TRANSMIT 0x11
#define UART_SET_DATA 0x12
#define UART_NONE 0x00

byte cmd, reg_status = 0x0E, bank = 0;
bool err = false, transmit = false;

byte data[2][DATA_LEN] = {{
                     0xFF, 0x0, 0xAD, 0x0,
                     0x40, 0x40, 0x40, 0x1A,
                     0x26, 0, 0, 0, 0, 0, 0,
                     0xAC
                    },
                    {
                     0xFF, 0x0, 0xAD, 0x0,
                     0x40, 0x40, 0x40, 0x1A,
                     0x26, 0, 0, 0, 0, 0, 0,
                     0xAC
                    }
                   };
byte data_sel = 0;

byte uart_cmd = UART_NONE;
byte data_write_sel = 1, data_write_byte = 0;

#ifdef DEBUG_ERR
byte l[10];
#endif

void setup (void)
{
  Serial.begin (115200);
  
  SPCR |= bit (SPE);
  pinMode(MISO, OUTPUT);
}

inline void read_byte() {
  SPI.transfer(0x00);
}

inline void read_bytes(byte len) {
  while (len--)
    SPI.transfer(0x00);
}

void loop (void)
{
  if (Serial.available()) {
    /*Serial.write(Serial.read());
    return;*/
    byte b = Serial.read();
    switch (uart_cmd) {
      case UART_SET_TRANSMIT:
        transmit = b;
        uart_cmd = UART_NONE;
        reg_status = transmit ? 0x40 : 0x0E;
        break;
      case UART_SET_DATA:
        data[data_write_sel][data_write_byte++] = b;
        if (data_write_byte == DATA_LEN) {
          data_sel = data_write_sel;
          uart_cmd = UART_NONE;
        }
        break;       
      default:
        uart_cmd = b;
        if (uart_cmd == UART_SET_DATA) {
          data_write_sel = data_sel ^ 1;
          data_write_byte = 0;
        }
    }
  }
  
  cmd = SPI.transfer(reg_status);
  if (err) {
    if (cmd == 0)
      return;
    err = false;
  }
  if (bank & 1) {
    if (cmd >= 0x20 && cmd <= 0x2D) {
      read_bytes(4);
      return;
    }
    if (cmd == 0x2E) {
      read_bytes(11);
      return;
    }
    if (cmd == 0x07) {
      SPI.transfer(reg_status);
      return;
    }
    if (cmd == 0x50 && SPI.transfer(0x00) == 0x53) {
      bank++;
      reg_status = 0x0E;
      return;
    }
  } else {
    if (cmd == 0x07) {
      SPI.transfer(reg_status);
      return;
    }
    if (cmd == 0x00) {
      SPI.transfer(0x0C);
      return;
    }
    if (cmd == 0x2A || cmd == 0x2B || cmd == 0x30) {
      read_bytes(5);
      return;
    }
    if (cmd == 0x25) {
      if (SPI.transfer(0x00) != 0x08 && transmit) {
        reg_status = 0x40;
      }
      return;
    }
    if (cmd == 0x27) {
      if (SPI.transfer(0x00) == 0x40) {
        reg_status = 0x40;
      }
      return;
    }
    if ((cmd >= 0x20 && cmd <= 0x29) ||
        (cmd >= 0x2C && cmd <= 0x2F) ||
        (cmd >= 0x31 && cmd <= 0x37)) {
      read_byte();
      return;
    }
    if (cmd == 0xE1 || cmd == 0xE2) {
      SPI.transfer(0x0E);
      return;
    }
    if (cmd == 0x50 && SPI.transfer(0x00) == 0x53) {
      bank++;
      reg_status = 0x8E;
      return;
    }
    if (cmd == 0x61) {
      for (int k = 0; k < 16; k++) {
#ifdef LOG_ERR
        if (SPI.transfer(data[k])) {
          Serial.print('?');
        }
#else
      SPI.transfer(data[data_sel][k]);
#endif
      }
      return;
    }
  }
  err = true;
  
#ifdef DEBUG_ERR
  for (byte i = 0; i < 10; i++)
    l[i] = SPI.transfer(0x00);
#endif
#ifdef LOG_ERR
  Serial.print('#');
#endif
#ifdef DEBUG_ERR
  Serial.print(cmd, HEX);
  Serial.print(" bank:");
  Serial.println(bank);
  for (byte i = 0; i < 10; i++) {
    Serial.print(l[i], HEX);
    Serial.print(' ');
  }
  Serial.println();
#endif
}
